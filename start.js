const puppeteer = require('puppeteer'); //Headless browser library
const fs = require('fs'); //FileSystem library

const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi; //Regex query to remove script tags and anything in between
const STYLE_REGEX = /<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi; //Regex query to remove style tags and anything in between
const { movieIDs } = require("./movies");
(async() => {
    //Creates movies folder if it doesn't exist
    if (!fs.existsSync('movies')) {
        fs.mkdirSync('movies');
    }

    // Launch the headless browser and open a new blank page
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Add user-agents without this imdb oage returns 403 Forbidden error (Their security check tries to prevent web scrapers)
    const userAgent = require('user-agents');
    await page.setUserAgent(userAgent.random().toString());

    //Downloads page resources and writes them into separate files, movie id is used as file name
    for (movieID of movieIDs) {
        await downloadMovieConnectionsSource(movieID);
    }

    await browser.close();

    async function downloadMovieConnectionsSource(movieID) {
        // Navigate the page to the imdb movieconnections page
        await page.goto(`https://m.imdb.com/title/${movieID}/movieconnections/`);

        // Click on all the see more buttons in the page
        const buttons = await page.$x("//button[contains(@class, 'ipc-see-more__button')]");
        for (const button of buttons) {
            await button.evaluate(b => b.click());
        }

        await delay(5000);

        //Get page source without images
        let source = await page.content({ "waitUntil": "domcontentloaded" });

        // Remove unnecessary tags to reduce the file size
        source = removeTags(source, SCRIPT_REGEX); //Remove <script> tag and anything in between
        source = removeTags(source, STYLE_REGEX); //Remove <style> tag and anything in between
        fs.writeFileSync(`./movies/${movieID}.html`, source);
        console.log(`${movieID} downloaded`);
    }

    function removeTags(pageSource, regex) {
        while (regex.test(pageSource)) {
            pageSource = pageSource.replace(regex, "");
        }
        return pageSource;
    }

    function delay(time) {
        return new Promise(function(resolve) {
            setTimeout(resolve, time)
        });
    }
})();