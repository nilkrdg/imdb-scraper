const htmlParser = require('node-html-parser');
const fs = require('fs');
const moviesFolderPath = './movies';
const resultFilePath = 'result.csv';

const parsedMovies = getParsedMovies();

parseFiles(parsedMovies);

function parseFiles(parsedMovies) {
    const logger = fs.createWriteStream(resultFilePath, {
        flags: 'a' // 'a' means appending (old data will be preserved)
    });

    //Read movies directory and parse each file
    fs.readdir(moviesFolderPath, (err, files) => {
        for (let index = 0; index < files.length; index++) {
            const fileName = files[index];
            const movieId = fileName.replace('.html', '');
            if (!parsedMovies.includes(movieId)) {
                const data = fs.readFileSync(moviesFolderPath + '/' + fileName, 'utf8');

                const root = htmlParser.parse(data); //load data with parser

                //Get each movie list with queries
                const followedByMovieIds = getMovieIds(root, 'sub-section-followed_by');
                const followsMovieIds = getMovieIds(root, 'sub-section-follows');
                const versionOfMovieIds = getMovieIds(root, 'sub-section-version_of');

                //Convert movie lists into CSV format
                const followedByMovieIdsStr = convertMovieArrayToStr(followedByMovieIds);
                const followsMovieIdsStr = convertMovieArrayToStr(followsMovieIds);
                const versionOfMovieIdsStr = convertMovieArrayToStr(versionOfMovieIds);

                //Write lists into the CSV file
                console.log(`writing index: ${index+1}, movieId: ${movieId}`);
                logger.write(`\n${movieId},${followedByMovieIdsStr},${followsMovieIdsStr},${versionOfMovieIdsStr}`);
            } else {
                console.log(`skipping movie already parsed, index: ${index+1},  movieId: ${movieId}`);
            }
        }
    });
}

function getParsedMovies() {
    const parsedMovieDataLines = fs.readFileSync(resultFilePath, 'utf8').toString().split('\n');
    const parsedMovies = [];
    if (parsedMovieDataLines.length > 1) {
        for (let index = 1; index < parsedMovieDataLines.length; index++) {
            const parsedMovieData = parsedMovieDataLines[index];
            parsedMovies.push(parsedMovieData.split(',')[0]);
        }
    }
    return parsedMovies;
}

function getMovieIds(root, query) {
    const followedByMovies = [];
    const followedByListWrapper = root.querySelector(`div[data-testid="${query}"]`);
    const followedByList = followedByListWrapper !== null ? followedByListWrapper.childNodes[0] : null;
    if (followedByList !== null) {
        for (const followedByListItem of followedByList.childNodes) {
            const movieLink = followedByListItem.querySelector('a[href]');
            const followedByData = movieLink !== null ? movieLink.getAttribute('href') : null;
            if (followedByData !== null) {
                followedByMovies.push(followedByData.replace('?ref_=ttcnn', '').replace('/title/', ''));
            }
        }
    }
    return followedByMovies;
}

function convertMovieArrayToStr(movieArray) {
    if (movieArray.length === 1) {
        return movieArray[0];
    } else if (movieArray.length > 1) {
        return `"${movieArray.toString()}"`;
    } else {
        return '';
    }
}