# IMDB Scraper

Downloads imdb page resources by using Puppeteer (headless browser)

## Getting Started

### Dependencies

* Follow the link to download Nodejs before following the below steps: https://nodejs.org/en/download

### Installing

* Install node modules by running the following command:
```
npm i
```

### Executing program
* Change movieIDs list which is in the movies.js file
* Run the following program in the terminal:
```
npm run start
```
The downloaded page resources will be written under ./movies folder. Move id is used as file name.